
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LucianoTadeu
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time t = new Time();
        Jogador j = new Jogador(0, "");
    
        j.setNumero(1); j.setNome("Jailson");
        t.addJogador("Goleiro", j);
        
        j.setNumero(1); j.setNome("Jailson");
        t.addJogador("Goleiro", j);
//        j.setNumero(1); j.setNome("Prass");
//        t2.addJogador("Goleiro", j);        
        
        j.setNumero(3); j.setNome("Vitor");
        t.addJogador("Volante", j);
//        j.setNumero(3); j.setNome("Edu");
//        t2.addJogador("Volante", j);        

        j.setNumero(5); j.setNome("Moises");
        t.addJogador("Meia", j);
//        j.setNumero(5); j.setNome("Cleiton");
//        t2.addJogador("Meia", j);        

        j.setNumero(11); j.setNome("Gabriel");
        t.addJogador("Atacante", j);
//        j.setNumero(11); j.setNome("Dudu");
//        t2.addJogador("Atacante", j);        

        j.setNumero(9); j.setNome("Barrios");
        t.addJogador("Centroavante", j);
//        j.setNumero(9); j.setNome("Alecsandro");
//        t2.addJogador("Centroavante", j);        
        
        HashMap<String, Jogador> jogs;
        jogs = t.getJogadores();
        
        System.out.println("Posição" + "\t\t" + "Time 1");
        Set<String> entries = jogs.keySet();
        for (String entry: entries) {
            System.out.println(entry + "\t\t" + jogs.get(entry).toString() );
        }
        
        // Ordem descendente de nome
        JogadorComparator cmpDescNome = new JogadorComparator(false, false, false);
        
        List<Jogador> lstjogs = new ArrayList<>();
        
        lstjogs = t.ordena(cmpDescNome);
        System.out.println("Time em Ordem Descendente de Nome");
        for (Jogador jog: lstjogs) {
            System.out.println(jog);
        }
        
        // Ordem ascendente de número
        JogadorComparator cmpAscNum = new JogadorComparator(true, true, false);
        lstjogs = t.ordena(cmpAscNum);
        System.out.println("Time em Ordem Ascendente de Número");
        for (Jogador jog: lstjogs) {
            System.out.println(jog);
        }
        
        // Use o método Collections.binarySearch para localizar um determinado 
        // jogador por número dentro da lista ordenada no item anterior. Exiba 
        // os dados do jogador localizado. Verifique se o resultado quando há 
        // dois ou mais jogadores com o mesmo número, mas com nomes diferentes, 
        // é correto.
        
        j.setNumero(9); // j.setNome("Gabriel");
        int indice = Collections.binarySearch(lstjogs, j, cmpAscNum);
        System.out.println("Jogador: " + indice);
        Jogador res = lstjogs.get(indice);
        System.out.println(res);
    }
}
