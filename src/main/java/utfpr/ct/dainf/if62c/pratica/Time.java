/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author LucianoTadeu
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    
    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    /* Na classe Time implemente o método público addJogador(String, Jogador) 
       que deverá incluir o jogador recebido como argumento no HashMap usando 
       como chave a posição informada no primeiro argumento.
    */
    public void addJogador(String pos, Jogador jog) {
        jogadores.put(pos, new Jogador(jog.getNumero(), jog.getNome()));
    }
    
    public List<Jogador> ordena(JogadorComparator comp) {
        List<Jogador> lista = new ArrayList<>();
        
        Set<String> entries = jogadores.keySet();
        for (String entry: entries) {
            lista.add(new Jogador(jogadores.get(entry).numero, jogadores.get(entry).nome));
        }
        // sort(List<T> l, Comparator<? super T> c)
        Collections.sort(lista, comp);
        return lista;
    }
}
