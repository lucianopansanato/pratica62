/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author LucianoTadeu
 */
public class JogadorComparator implements Comparator<Jogador>{
    // Esta classe deverá ter a opção de comparar em ordem ascendente ou 
    // descendente de número e/ou nome, isto é, quando comparando por número, 
    // se forem iguais, comparar pelo nome, quando comparando por nome, 
    // se forem iguais, comparar pelo número.
    // Esta classe deve implementar um construtor que receba três valores 
    // boolean: o primiero, se verdadeiro, indica ordenação por número, 
    // o segundo, se verdadeiro, indica que desejamos ordenar por número 
    // ascendente e o terceiro argumento, se verdadeiro, indica que desejamos 
    // ordenar por nome ascendente. O construtor padrão deve ser equivalente 
    // a ordenar por número e nome em ordem ascendente.
    private boolean ordNum; // true indica ordenação por número
    private boolean numAsc; // true indica ordenar por número ascendente
    private boolean nomAsc; // true indica ordenar por nome ascendente
    
    public JogadorComparator() {
        ordNum = true;
        numAsc = true;
        nomAsc = true;
    }
    
    public JogadorComparator(boolean ordNum, boolean numAsc, boolean nomAsc) {
        this.ordNum = ordNum;
        this.numAsc = numAsc;
        this.nomAsc = nomAsc;
    }
    
    @Override
    public int compare(Jogador o1, Jogador o2) {
        if (ordNum) {
            // ordenação por número
            if (numAsc) {
                // número ascendente
                return o1.numero - o2.numero;
            }
            else {
                // número descendente
                return (-1 * o1.numero - o2.numero);
            }
        }
        else {
            // ordenação por nome
            if (nomAsc) {
                // nome ascendente
                return o1.nome.compareTo(o2.nome);
            }
            else {
                // nome descendente
                return (-1 * o1.nome.compareTo(o2.nome));
            }
        }
    }
}
